package app.akexorcist.tcpsocket;

import org.json.JSONArray;

/**
 * Created by USER on 30/11/2559.
 */

public class ServerData {
    private static ServerData instance;
    private static JSONArray _allUser;
    private static String etxtIP = "";
    private static String etxtDname = "";

    public static ServerData getInstance(){
        if(instance == null){
            instance = new ServerData();
        }
        return instance;
    }

    public void set_allUser(JSONArray obj){
        _allUser = obj;
    }

    public JSONArray get_allUser(){
        return _allUser;
    }

    public void setOtherClientIp(String ip) {
        etxtIP = ip;
    }

    public String getOtherClientIp(){
        return etxtIP;
    }

    public void setOtherClientDname(String dname) {
        etxtDname = dname;
    }

    public String getOtherClientDname(){
        return etxtDname;
    }
}
