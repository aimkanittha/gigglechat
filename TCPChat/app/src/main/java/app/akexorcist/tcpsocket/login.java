package app.akexorcist.tcpsocket;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.json.JSONArray;

public class login extends Activity {
    EditText username, password, dname;
    TextView alert, warn;
    Button submit;
    String displayName, uri;
    static String SERVER_URL = "http://"+AppConstants.SERVER_IP+":"+AppConstants.SERVER_PORT;
    public static String user, pwd, ipAddr;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        username = (EditText) findViewById(R.id.user);
        password = (EditText) findViewById(R.id.pwd);
        dname = (EditText) findViewById(R.id.dname);
        alert = (TextView) findViewById(R.id.alert);
        warn = (TextView) findViewById(R.id.warning);
        submit = (Button) findViewById(R.id.submit);
        alert.setVisibility(View.INVISIBLE);
        warn.setVisibility(View.INVISIBLE);

        int SDK_INT = android.os.Build.VERSION.SDK_INT;
        if (SDK_INT > 8)
        {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                    .permitAll().build();
            StrictMode.setThreadPolicy(policy);

            submit.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {

                    ServerConnection server = new ServerConnection();

                    if(!isNetworkAvailable())
                    {
                        Log.d("AIM said","Cannot connect with Network");
                    }

                    user = username.getText().toString();
                    pwd = password.getText().toString();
                    displayName = dname.getText().toString();
                    displayName = displayName.equals("") ? "NULL" : displayName ;

                    if(user.equals("") || pwd.equals("")){
                        alert.setVisibility(View.VISIBLE);
                        return;
                    }else{
                        alert.setVisibility(View.INVISIBLE);
                    }
                    ipAddr = getIP();

                    uri = SERVER_URL + AppConstants.LINK_LOGIN + user
                            + AppConstants.CONNECTOR + pwd + AppConstants.CONNECTOR
                            + ipAddr + AppConstants.CONNECTOR + displayName;

                    String results = "";
                    try {
                        results = server.doHttpUrlConnectionAction(uri);
                        if(results.equals("4004") || results == null) // NOT FOUND
                        {
                            warn.setVisibility(View.VISIBLE);
                            return;
                        }else {
                            warn.setVisibility(View.INVISIBLE);
                            ServerData data = ServerData.getInstance();
                            data.set_allUser(JsonReader.readJson(results));

                            JSONArray jsonObject = data.get_allUser();
                            Log.i("!!!!!!!!!","555555555555555");

                            Intent i=new Intent(login.this, ShowFriends.class);
                            startActivity(i);
                        }
                    } catch (Exception e) {
                        AlertDialog show = new AlertDialog.Builder(login.this)
                                .setTitle("Warning!")
                                .setMessage("Cannot access to server, please re-connect your internet")
                                .setNeutralButton("OK", null)
                                .show();
                    }
                }
            });
        }
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
    // check wifi and check mobile internet connection
    private boolean haveNetworkConnection() {
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;

        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
        for (NetworkInfo ni : netInfo) {
            if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                if (ni.isConnected())
                    haveConnectedWifi = true;
            if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                if (ni.isConnected())
                    haveConnectedMobile = true;
        }
        return haveConnectedWifi || haveConnectedMobile;
    }

    private String getIP(){
        WifiManager wifiManager = (WifiManager) getSystemService(WIFI_SERVICE);
        WifiInfo wifiInfo = wifiManager.getConnectionInfo();
        int ipAddress = wifiInfo.getIpAddress();
        String ip = (ipAddress & 0xFF) + "." +
                ((ipAddress >> 8 ) & 0xFF) + "." +
                ((ipAddress >> 16 ) & 0xFF) + "." +
                ((ipAddress >> 24 ) & 0xFF );
        if(ip == "0.0.0.0"){
            ip = getIP(); // try again
        }
        return ip;
    }
}

