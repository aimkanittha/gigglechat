package app.akexorcist.tcpsocket;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

public class Main extends Activity {
			private static final int REQUEST_PATH = 1;
			String curFileName;
			public static final int TCP_SERVER_PORT = 21111;
			String Mpath,b, etxtDname, etxtIP;
			boolean chk_sendfile;

			TextView txtIP, txtStatus;
			ListView listChat;
			Button btnSend, sendFileBtn;
			EditText etxtMessage;

			ArrayList<String> arr_list;
			List<Integer> arr_gravity;

			InService inTask;

		public void onCreate(Bundle savedInstanceState) {
			super.onCreate(savedInstanceState);
			setContentView(R.layout.main);
			sendFileBtn = (Button) findViewById(R.id.sendFileBtn_group);
			arr_list = new ArrayList<String>();
			arr_gravity = new ArrayList<Integer>();
			listChat = (ListView)findViewById(R.id.listChat);
//		etxtIP = (EditText)findViewById(R.id.etxtIP);

			txtStatus = (TextView)findViewById(R.id.txtStatus);
			etxtIP = ServerData.getInstance().getOtherClientIp();
			etxtDname = ServerData.getInstance().getOtherClientDname();
			txtIP = (TextView)findViewById(R.id.txtIP);
			txtIP.setText("PARTNER : " + etxtDname);
//        SharedPreferences settings = getSharedPreferences("Pref", 0);
//        String ip = settings.getString("IP", "192.168.1.1");
//        etxtIP.setText(ip);
			etxtMessage = (EditText)findViewById(R.id.etxtMessage);

//		SharedPreferences settings = getSharedPreferences("Pref", 0);
//		String ip = settings.getString("IP", "192.168.1.1");
//		etxtIP.setText(ip);

			inTask = new InService(getApplicationContext()
					, TCP_SERVER_PORT, listChat, arr_list
					, arr_gravity);
			inTask.execute();

			btnSend = (Button) findViewById(R.id.btnSend);


			btnSend.setOnClickListener(new View.OnClickListener() {
				public void onClick(View v) {

					if(chk_sendfile==false) {

						if (etxtMessage.getText().toString().length() > 0) {

							txtStatus.setText("Sending...");
							sendMessage(etxtIP.toString()
									, etxtMessage.getText().toString());
							etxtMessage.setText("");
						}
					}
					else {
						txtStatus.setText("SendingFile...");
						sendfile(etxtIP.toString());
					}
				}
			});
		}

		public void getfile(View view){
			Intent intent1 = new Intent(this, FileChooser.class);
			startActivityForResult(intent1,REQUEST_PATH);

		}
		// Listen for results.
		@Override
		protected void onActivityResult(int requestCode, int resultCode, Intent data){
			// See which child activity is calling us back.
			if (requestCode == REQUEST_PATH){
				if (resultCode == RESULT_OK) {
					chk_sendfile=true;
					curFileName = data.getStringExtra("GetFileName");
					curFileName = data.getStringExtra("GetPath");
					b=data.getStringExtra("GetFileName");
					Mpath=curFileName+"/"+b;
					System.out.print(Mpath);
					etxtMessage.setText(curFileName+"/"+b);
				}
			}
		}

		public void sendMessage(String ip, String message) {
			final String IP_ADDRESS = ip;
			final String MESSAGE = message;

			Runnable runSend = new Runnable() {
				public void run() {
					try {
						Socket s = new Socket(IP_ADDRESS, TCP_SERVER_PORT);
						arr_gravity.add(Gravity.LEFT);
						arr_list.add("YES");

						s.setSoTimeout(5000);
						BufferedReader in = new BufferedReader(new InputStreamReader(s.getInputStream()));
						BufferedWriter out = new BufferedWriter(new OutputStreamWriter(s.getOutputStream()));
						String outgoingMsg = MESSAGE + System.getProperty("line.separator");

						out.write(outgoingMsg);
						out.flush();
						Log.i("TcpClient", "sent: " + outgoingMsg);
						String inMsg = in.readLine() + System.getProperty("line.separator");

						Handler refresh = new Handler(Looper.getMainLooper());
						refresh.post(new Runnable() {
							public void run() {
								arr_gravity.add(Gravity.LEFT);

								arr_list.add("Me : "+MESSAGE);
								listChat.setAdapter(new CustomListViewBlack(getApplicationContext()
										, android.R.layout.simple_list_item_1, arr_list, arr_gravity));
								listChat.setSelection(listChat.getCount());
								txtStatus.setText("Message has been sent.");
								etxtMessage.setText("");
							}
						});

						//Log.i("Message Response", inMsg);
						s.close();
					} catch (UnknownHostException e) {
						e.printStackTrace();
						setText("No device on this IP address.");
					} catch (Exception e) {
						e.printStackTrace();
						setText("Connection failed. Please try again.");
					}
				}

				public void setText(String str) {
					final String string = str;
					Handler refresh = new Handler(Looper.getMainLooper());
					refresh.post(new Runnable() {
						public void run() {
							txtStatus.setText(string);
						}
					});
				}
			};
			new Thread(runSend).start();
		}

		public void sendfile(String ip){
			final String a=ip;
			Runnable runsendfile = new Runnable() {
				public void run() {
					try {
						Socket s = new Socket(a, TCP_SERVER_PORT);
						Socket sendPath = new Socket(a,8888);

						s.setSoTimeout(5000);
						File file = new File(Mpath);
						long length = file.length();
						byte[] bytes = new byte[16 * 1024];
						InputStream in = new FileInputStream(file);
						OutputStream out = s.getOutputStream();

						int count;
						while ((count = in.read(bytes)) > 0) {

							out.write(bytes, 0, count);
						}

						//txtStatus.setText("Complete");
						out.flush();
						s.close();

						BufferedReader ins = new BufferedReader(new InputStreamReader(sendPath.getInputStream()));
						BufferedWriter outs = new BufferedWriter(new OutputStreamWriter(sendPath.getOutputStream()));
						outs.write(b);
						outs.flush();
						sendPath.close();

						Handler refresh = new Handler(Looper.getMainLooper());
						refresh.post(new Runnable() {
							public void run() {
								arr_gravity.add(Gravity.LEFT);

								arr_list.add("Me : Sent file "+"'"+b+"'"+"complete");
								listChat.setAdapter(new CustomListViewBlack(getApplicationContext()
										, android.R.layout.simple_list_item_1, arr_list, arr_gravity));
								listChat.setSelection(listChat.getCount());
								txtStatus.setText("File sent complete");
								etxtMessage.setText("");
								chk_sendfile=false;
							}
						});

					} catch (UnknownHostException e) {
						e.printStackTrace();
						//txtStatus.setText("No device on this IP address.");
					} catch (Exception e) {
						e.printStackTrace();
						//txtStatus.setText("Connection failed. Please try again.");
					}


				}
			};
			new Thread(runsendfile).start();

		}

		public String getIP() {
			WifiManager wifiManager = (WifiManager) getSystemService(WIFI_SERVICE);
			WifiInfo wifiInfo = wifiManager.getConnectionInfo();
			int ipAddress = wifiInfo.getIpAddress();
			String ip = (ipAddress & 0xFF) + "." +
					((ipAddress >> 8 ) & 0xFF) + "." +
				((ipAddress >> 16 ) & 0xFF) + "." +
				((ipAddress >> 24 ) & 0xFF );
		if(ip.equals("0.0.0.0"))
			ip = "Please connect WIFI";
		return ip;
	}

	public void onPause() {
		super.onPause();
		SharedPreferences settings = getSharedPreferences("Pref", 0);
		SharedPreferences.Editor editor = settings.edit();
		editor.putString("IP", etxtIP.toString());
		editor.commit();
		inTask.killTask();
	}


//	private static final int REQUEST_PATH = 1;
//    public static final int TCP_SERVER_PORT = 21111;
//	Main instance;
//
//	String curFileName;
//	String Mpath,b;
//	boolean chk_sendfile;
//
//	TextView txtIP, txtStatus;
//	ListView listChat;
//	Button btnSend;
//	EditText etxtMessage;
//	ArrayList<String> arr_list;
//	List<Integer> arr_gravity;
//	InService inTask;
//	String etxtIP , etxtDname;
//
//    public void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.main);
//
//		InService inTask;
//
//        arr_list = new ArrayList<String>();
//        arr_gravity = new ArrayList<Integer>();
//        listChat = (ListView)findViewById(R.id.listChat);
////        etxtIP = (EditText)findViewById(R.id.etxtIP);
//        etxtMessage = (EditText)findViewById(R.id.etxtMessage);
//        txtIP = (TextView)findViewById(R.id.txtIP);
//        txtStatus = (TextView)findViewById(R.id.txtStatus);
//		etxtIP = ServerData.getInstance().getOtherClientIp();
//		etxtDname = ServerData.getInstance().getOtherClientDname();
//		txtIP.setText("PARTNER : " + etxtDname);
////        SharedPreferences settings = getSharedPreferences("Pref", 0);
////        String ip = settings.getString("IP", "192.168.1.1");
////        etxtIP.setText(ip);
//
//        inTask = new InService(getApplicationContext()
//        			, TCP_SERVER_PORT, listChat, arr_list
//        			, arr_gravity);
//        inTask.execute();
//
//        btnSend = (Button) findViewById(R.id.btnSend);
//        btnSend.setOnClickListener(new OnClickListener() {
//        	public void onClick(View v) {
//        		if(etxtMessage.getText().toString().length() > 0) {
//
//					txtStatus.setText("Sending...");
//        			sendMessage(etxtIP
//	        				, etxtMessage.getText().toString());
//	        		etxtMessage.setText("");
//        		}
//
//
////				SENDING FILE
//				if(chk_sendfile==false) {
//
//					if (etxtMessage.getText().toString().length() > 0) {
//
//						txtStatus.setText("Sending...");
//						sendMessage(etxtIP.getText().toString()
//								, etxtMessage.getText().toString());
//						etxtMessage.setText("");
//					}
//				}
//				else {
//					txtStatus.setText("SendingFile...");
//					sendfile(etxtIP.getText().toString());
//				}
//        	}
//        });
//    }
//
//    public void sendMessage(String ip, String message) {
//					final String IP_ADDRESS = ip;
//					final String MESSAGE = message;
//
//					Runnable runSend = new Runnable() {
//						public void run() {
//							try {
//								Socket s = new Socket(IP_ADDRESS, TCP_SERVER_PORT);
//								s.setSoTimeout(5000);
//								BufferedReader in = new BufferedReader(new InputStreamReader(s.getInputStream()));
//								BufferedWriter out = new BufferedWriter(new OutputStreamWriter(s.getOutputStream()));
//								String outgoingMsg = MESSAGE + System.getProperty("line.separator");
//								out.write(outgoingMsg);
//								out.flush();
//					Log.i("TcpClient", "sent: " + outgoingMsg);
//					String inMsg = in.readLine() + System.getProperty("line.separator");
//
//					Handler refresh = new Handler(Looper.getMainLooper());
//					refresh.post(new Runnable() {
//						public void run() {
//							arr_gravity.add(Gravity.LEFT);
//        					arr_list.add("ME : " + MESSAGE);
//        					listChat.setAdapter(new CustomListViewBlack(getApplicationContext()
//        		    				, android.R.layout.simple_list_item_1, arr_list, arr_gravity));
//        					listChat.setSelection(listChat.getCount());
//        					txtStatus.setText("Message has been sent.");
//        	        		etxtMessage.setText("");
//						}
//					});
//
//					Log.i("Message Response", inMsg);
//					s.close();
//				} catch (UnknownHostException e) {
//					e.printStackTrace();
//					setText("No device on this IP address.");
//				} catch (Exception e) {
//					e.printStackTrace();
//					setText("Connection failed. Please try again.");
//				}
//			}
//
//			public void setText(String str) {
//				final String string = str;
//		    	Handler refresh = new Handler(Looper.getMainLooper());
//				refresh.post(new Runnable() {
//					public void run() {
//						txtStatus.setText(string);
//					}
//				});
//			}
//		};
//		new Thread(runSend).start();
//    }
//
//
//	public void sendfile(String ip){
//		final String a=ip;
//		Runnable runsendfile = new Runnable() {
//			public void run() {
//				try {
//					Socket s = new Socket(a, TCP_SERVER_PORT);
//					Socket sendPath = new Socket(a,8888);
//
//					s.setSoTimeout(5000);
//					File file = new File(Mpath);
//					long length = file.length();
//					byte[] bytes = new byte[16 * 1024];
//					InputStream in = new FileInputStream(file);
//					OutputStream out = s.getOutputStream();
//
//					int count;
//					while ((count = in.read(bytes)) > 0) {
//
//						out.write(bytes, 0, count);
//					}
//
//					//txtStatus.setText("Complete");
//					out.flush();
//					s.close();
//
//					BufferedReader ins = new BufferedReader(new InputStreamReader(sendPath.getInputStream()));
//					BufferedWriter outs = new BufferedWriter(new OutputStreamWriter(sendPath.getOutputStream()));
//					outs.write(b);
//					outs.flush();
//					sendPath.close();
//
//					Handler refresh = new Handler(Looper.getMainLooper());
//					refresh.post(new Runnable() {
//						public void run() {
//							arr_gravity.add(Gravity.LEFT);
//
//							arr_list.add("Me : Sent file "+"'"+b+"'"+"complete");
//							listChat.setAdapter(new CustomListViewBlack(getApplicationContext()
//									, android.R.layout.simple_list_item_1, arr_list, arr_gravity));
//							listChat.setSelection(listChat.getCount());
//							txtStatus.setText("File sent complete");
//							etxtMessage.setText("");
//							chk_sendfile=false;
//						}
//					});
//
//
//
//
//				} catch (UnknownHostException e) {
//					e.printStackTrace();
//					//txtStatus.setText("No device on this IP address.");
//				} catch (Exception e) {
//					e.printStackTrace();
//					//txtStatus.setText("Connection failed. Please try again.");
//				}
//
//
//			}
//		};
//		new Thread(runsendfile).start();
//
//	}
//
//    public String getIP() {
//    	WifiManager wifiManager = (WifiManager) getSystemService(WIFI_SERVICE);
//    	WifiInfo wifiInfo = wifiManager.getConnectionInfo();
//    	int ipAddress = wifiInfo.getIpAddress();
//    	String ip = (ipAddress & 0xFF) + "." +
//    			((ipAddress >> 8 ) & 0xFF) + "." +
//    			((ipAddress >> 16 ) & 0xFF) + "." +
//                ((ipAddress >> 24 ) & 0xFF );
//    	if(ip.equals("0.0.0.0"))
//    		ip = "Please connect WIFI";
//    	return ip;
//	}
//
//    public void onPause() {
//    	super.onPause();
//    	SharedPreferences settings = getSharedPreferences("Pref", 0);
//        SharedPreferences.Editor editor = settings.edit();
//        editor.putString("IP", etxtIP);
//        editor.commit();
//        inTask.killTask();
//    }
}
