package app.akexorcist.tcpsocket;

import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.net.ConnectException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by USER on 30/11/2559.
 */
public class ServerConnection {

    public HttpURLConnection isConnectedToServer(String url, int timeout) {
        try{
//            url = URLEncoder.encode(url, "UTF-8");
            URL myUrl = new URL(url);
            HttpURLConnection connection = (HttpURLConnection) myUrl.openConnection();
            connection.setConnectTimeout(timeout);
            connection.connect();
            return connection;
        } catch (Exception e) {
            return null;
        }
    }

    public String doHttpUrlConnectionAction(String desiredUrl)
            throws ConnectException, IOException
    {
        HttpURLConnection conn = null;
        BufferedReader br = null;
        String output = "";
        try{
                URL url = new URL(desiredUrl);
                conn = (HttpURLConnection) url.openConnection();
                conn.setRequestMethod(AppConstants.METHOD_GET);
                conn.setRequestProperty("Accept", "application/json");

                if (conn.getResponseCode() != 200) {
//                    Log.d("!!!!!!!!!!!!!" , conn.getResponseCode() + "");
//                    throw new RuntimeException("Failed : HTTP error code : "
//                            + conn.getResponseCode());
                    Log.d("!!!!!!!!!!!!!" , conn.getResponseCode() + "");
                }

                br = new BufferedReader(new InputStreamReader(
                        (conn.getInputStream())));

                String o = "";
                System.out.println("Output from Server .... \n");
                while ((o = br.readLine()) != null) {
                    if(!o.equals("") || o != null)
                    {
                        System.out.println(o);
                        output += o;
                    }
                }

            } catch (MalformedURLException e) {

                e.printStackTrace();
                return null;

            } catch (IOException e) {

                e.printStackTrace();
                return null;
        }finally {
            if(conn != null)
                conn.disconnect();
            if(br != null)
                br.close();
        }

        return output;
    }


        //                URL url = null;
//                BufferedReader reader = null;
//                StringBuilder stringBuilder;
//                HttpURLConnection conn = null;
//        try
//        {
//            conn = isConnectedToServer(desiredUrl, 15000);
//            if(conn == null)
//            {
//                Log.d("AIM said","Already connected to Server");
//                return "Cannot access to Server";
//            }
//
//            InputStream is = null;
//            // Only display the first 500 characters of the retrieved
//            // web page content.
//            int len = 500;
//
//            try {
//                url = new URL(desiredUrl);
//                conn = (HttpURLConnection) url.openConnection();
//                int response = conn.getResponseCode();
//                Log.d("DEBUG_TAG", "The response is: " + response);
//                is = conn.getInputStream();
//
//                // Convert the InputStream into a string
//
//                Log.d("HEY","Congratulation");
//                String contentAsString = readIt(is, len);
//                return contentAsString;
//
//                // Makes sure that the InputStream is closed after the app is
//                // finished using it.
//            } finally {
//                if (is != null) {
//                    is.close();
//                }
//            }
//        }
//        catch (Exception e)
//        {
//            e.printStackTrace();
//            throw e;
//        }


    private String readIt(InputStream stream, int len) throws IOException, UnsupportedEncodingException {
        Reader reader = null;
        reader = new InputStreamReader(stream, "UTF-8");
        char[] buffer = new char[len];
        reader.read(buffer);
        return new String(buffer);
    }

}
