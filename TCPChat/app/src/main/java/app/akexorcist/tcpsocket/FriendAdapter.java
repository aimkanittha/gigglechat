package app.akexorcist.tcpsocket;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * Created by USER on 1/12/2559.
 */
public class FriendAdapter extends BaseAdapter {
    private Activity activity;
    private List<String> teacher_name;
    private List<String> subject;

    public FriendAdapter(Activity activity, List<String> teacher_name,  List<String> subject) {
        this.activity = activity;
        this.teacher_name = teacher_name;
        this.subject = subject;
    }


    @Override
    public int getCount() {
        return teacher_name.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        Holder holder;
        if (convertView == null) {
            LayoutInflater inflater = (activity).getLayoutInflater();
            convertView = inflater.inflate(R.layout.layout_listview_items, parent,false);
            holder = new Holder(convertView);
            convertView.setTag(holder);

        } else {
            holder = (Holder) convertView.getTag();
        }

        holder.name_teacher.setText(teacher_name.get(position));
        holder.subject.setText(subject.get(position));
        return convertView;
    }
        public class Holder{
            private TextView name_teacher;
            private TextView subject;
//            private Button ntBtn;
            public Holder(View v){
                name_teacher = (TextView)v.findViewById(R.id.teacher_name);
                subject = (TextView)v.findViewById(R.id.subject);
//                ntBtn = (Button) v.findViewById(R.id.nextBtn);
            }
        }
}
