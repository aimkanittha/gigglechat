package app.akexorcist.tcpsocket;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.IOException;
import java.io.Reader;

public class JsonReader {
    // GET BUFFERREDREADER
    public static String readAll(Reader rd) throws IOException {
        StringBuilder sb = new StringBuilder();
        int cp;

        while ((cp = rd.read()) != -1) {
            sb.append((char) cp);
        }
        return sb.toString();
    }

    public static JSONArray readJson(String jsonString) throws IOException, JSONException {
        try {
            JSONArray  json = new JSONArray(jsonString);
            return json;
        }catch (Exception e){
            Log.d("ERROR", e.getMessage());
        }
        return null;
    }
}
