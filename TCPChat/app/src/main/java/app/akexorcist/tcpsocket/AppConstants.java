package app.akexorcist.tcpsocket;

/**
 * Created by USER on 30/11/2559.
 */
public class AppConstants {
    // Server ip
    public static String SERVER_IP = "192.168.174.1";
    public static int SERVER_PORT = 7777;
    public static String METHOD_GET = "GET";
    public static String METHOD_SET = "SET";
    public static String LINK_REGISTER = "/register/";
    public static String LINK_LOGIN = "/login/";
    public static String CONNECTOR = "&&";
}
