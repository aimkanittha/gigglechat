package app.akexorcist.tcpsocket;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class ShowFriends extends Activity implements TextWatcher {
        private HashMap<String, Object> pairs = new HashMap<String, Object>();
        private FriendAdapter friend;
        private ListView lv_teacher;
        private Button refreshBtn, groupBtn;
        private static List<String> teacher_name;
        private login log = new login();
        public static ArrayList<String> ips;
//        public static boolean isChatGroup;
    private static List<String> subject;
        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_show_friends);
            refreshBtn = (Button) findViewById(R.id.refreshBtn);
            groupBtn = (Button) findViewById(R.id.groupBtn);
//            isChatGroup = false;
            try{
                teacher_name = new ArrayList<String>();
                subject = new ArrayList<String>();
                ips = new ArrayList<String>();
                findFriend();

                groupBtn.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
//                        isChatGroup = true;
                        Intent iCall = new Intent(ShowFriends.this,worldGroup.class);
                        startActivity(iCall);
                    }
                });

                if(pairs.size() != 0){
                    lv_teacher = (ListView) findViewById(R.id.listViewContainer);
//                    teacher_name = {"1","2","3","4"};
//                    subject = {"a","b","z","y"};
                    friend = new FriendAdapter(this,teacher_name,subject);
                    lv_teacher.setAdapter(friend);
//                    isChatGroup = false;
                    lv_teacher.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                            Log.i("thissss", subject.get(i));
                            ServerData.getInstance().setOtherClientIp(subject.get(i));
                            ServerData.getInstance().setOtherClientDname(teacher_name.get(i));

                            Intent iCall = new Intent(ShowFriends.this,Main.class);
                            startActivity(iCall);
                        }
                    });
                }
            }catch (Exception e){
                e.getStackTrace();
                Log.i("!!!!!!!!!!!!!", e.getMessage());
            }

            refreshBtn.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    if(isNetworkAvailable()){
                        try {
                            refreshFriend();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }else{
                        AlertDialog show = new AlertDialog.Builder(ShowFriends.this)
                                .setTitle("Required internet!")
                                .setMessage("Failed to get new friend IP address, Please connect your internect")
                                .setNeutralButton("OK", null)
                                .show();
                    }
                }
            });
        }

    private void refreshFriend() throws IOException{

        ServerConnection server = new ServerConnection();

        String user = log.user;
        String pwd = log.pwd;
        String displayName = "NULL";
        String ipAddr = log.ipAddr;

        String uri = log.SERVER_URL + AppConstants.LINK_LOGIN + user
                + AppConstants.CONNECTOR + pwd + AppConstants.CONNECTOR
                + ipAddr + AppConstants.CONNECTOR + displayName;

        String results = "";
        try {
            results = server.doHttpUrlConnectionAction(uri);
            if(results.equals("4004") || results == null) // NOT FOUND
            {
                AlertDialog show = new AlertDialog.Builder(ShowFriends.this)
                        .setTitle("Warning !!!")
                        .setMessage("Cannot get friend, please retry again !")
                        .setNeutralButton("OK", null)
                        .show();
            }else {
                ServerData data = ServerData.getInstance();
                data.set_allUser(JsonReader.readJson(results));

                JSONArray jsonObject = data.get_allUser();

                // Reload this page
                onRestart();
            }
        }catch (JSONException e){
            e.getStackTrace();
        }
    }

    private void findFriend() throws JSONException, UnsupportedEncodingException, GeneralSecurityException {
        JSONArray jArray = (JSONArray) ServerData.getInstance().get_allUser();

        for (int i = 0; i < jArray.length(); i++) {
            JSONObject j = jArray.optJSONObject(i);
            pairs.put(i + "",jsonToMap(j));
        }
    }

    public static Map<String, Object> jsonToMap(JSONObject json) throws JSONException, UnsupportedEncodingException, GeneralSecurityException {
        Map<String, Object> retMap = new HashMap<String, Object>();

        if(json != JSONObject.NULL) {
            retMap = toMap(json);
        }
        return retMap;
    }

    public static Map<String, Object> toMap(JSONObject object) throws JSONException, UnsupportedEncodingException, GeneralSecurityException {
        Map<String, Object> map = new HashMap<String, Object>();
        Iterator<String> keysItr = object.keys();
        while(keysItr.hasNext()) {
            String key = keysItr.next();
            Object value = object.get(key);

            if(value instanceof JSONArray) {
                value = toList((JSONArray) value);
            }

            else if(value instanceof JSONObject) {
                value = toMap((JSONObject) value);
            }
            map.put(key, value);

            if(key.equals("displayName")){
//                teacher_name.add(Crypto.decrypted(String.valueOf(value)));
                teacher_name.add(String.valueOf(value));
            }
            else if(key.equals("ip")){
//                subject.add(Crypto.decrypted(String.valueOf(value)));
//                ips.add(Crypto.decrypted(String.valueOf(value)));
                subject.add(String.valueOf(value));
                ips.add(String.valueOf(value));
            }
        }
        return map;
    }

    public static List<Object> toList(JSONArray array) throws JSONException, UnsupportedEncodingException, GeneralSecurityException {
        List<Object> list = new ArrayList<Object>();
        for(int i = 0; i < array.length(); i++) {
            Object value = array.get(i);
            if(value instanceof JSONArray) {
                value = toList((JSONArray) value);
            }

            else if(value instanceof JSONObject) {
                value = toMap((JSONObject) value);
            }
            list.add(value);
        }
        return list;
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    @Override
    protected void onRestart() {

        // TODO Auto-generated method stub
        super.onRestart();
        Intent i = new Intent(ShowFriends.this, ShowFriends.class);  //your class
        startActivity(i);
        finish();

    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void afterTextChanged(Editable editable) {

    }
}
