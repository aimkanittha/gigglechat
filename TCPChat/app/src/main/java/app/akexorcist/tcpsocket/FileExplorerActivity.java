package app.akexorcist.tcpsocket;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Created by USER on 10/12/2559.
 */
public class FileExplorerActivity  extends Activity {
    private static final int REQUEST_PATH = 1;
    TextView tt;
    String curFileName,Mpath;
    Button btsend,btr;
    EditText edittext,etextip,etextport;
    TextView a;
    EditText ip;

    static ServerSocket serv=null;
    static Socket soc=null;
    static OutputStream outr=null;
    static InputStream inr=null;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        /*super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fileexplorer);

        edittext = (EditText)findViewById(R.id.editText);
        etextip=(EditText)findViewById(R.id.eText2);
        btsend=(Button) findViewById(R.id.button3);
        btr=(Button)findViewById(R.id.button6);
        a=(TextView)findViewById(R.id.tv2);
*/
       /* int SDK_INT = android.os.Build.VERSION.SDK_INT;
        if (SDK_INT > 8) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                    .permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }*/
        //  ip=(EditText)findViewById(R.id.eText2);
          /*  btsend.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {

                    try {
                        String a = ip.getText().toString();
                        Socket s = new Socket(a, 21111);
                        s.setSoTimeout(5000);
                        File file = new File(Mpath);
                        long length = file.length();
                        byte[] bytes = new byte[16 * 1024];
                        InputStream in = new FileInputStream(file);
                        OutputStream out = s.getOutputStream();

                        int count;
                        while ((count = in.read(bytes)) > 0) {
                            out.write(bytes, 0, count);
                        }

                        tt.setText("Complete");
                        out.flush();
                        s.close();
                    } catch (UnknownHostException e) {
                        e.printStackTrace();
                        a.setText("No device on this IP address.");
                    } catch (Exception e) {
                        e.printStackTrace();
                        a.setText("Connection failed. Please try again.");
                    }

                }});



//        finish();
        tt=(TextView)findViewById(R.id.textView);
        btr.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                try {
                    serv=new ServerSocket(21111);
                    soc=serv.accept();
                    inr=soc.getInputStream();
                    outr=new FileOutputStream("/storage/extSdCard/a.jpg");
                    int size=soc.getReceiveBufferSize();
                    byte[] buff=new byte[size];

                    int len=-1;
                    while ((len=inr.read(buff))!=-1)
                    {
                        outr.write(buff,0,len);
                    }
                    tt.setText("Recieved");
                    System.out.print("recieve");
                    outr.close();
                    inr.close();
                    soc.close();

                } catch (SocketException e1) {
                    e1.printStackTrace();
                } catch (FileNotFoundException e1) {
                    e1.printStackTrace();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }});*/


    }

    public void getfile(View view){
        Intent intent1 = new Intent(this, FileChooser.class);
        startActivityForResult(intent1,REQUEST_PATH);
    }


    // Listen for results.
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        // See which child activity is calling us back.
        if (requestCode == REQUEST_PATH){
            if (resultCode == RESULT_OK) {
                //curFileName = data.getStringExtra("GetFileName");
                curFileName = data.getStringExtra("GetPath");
                String b=data.getStringExtra("GetFileName");
                edittext.setText(b);
                Mpath=edittext.getText().toString();
            }
        }
    }
}
